# Trellis Auto Deploy

Auto-deployment CI for Trellis staging / prod environments + optional whitesource renovate auto-merge

# Features
- Commits in trellis directory triggers provision + deploy
- Commits in site directory triggers only deploy
- Commits in main or master branches will target staging environment
- Commits in production branch will target to production
- Optional : Dependencies updates automerged (minor/patch) or suggested as MR (major) by whitesource renovate

## Installation
1. Add these files to your Trellis/Bedrock project
2. Create a new ssh key for Gitlab CI to be able to deploy without uploading your own key and add it to your trellis/group_vars/all/users.yml so it looks like this :
```
users:
  - name: "{{ web_user }}"
    groups:
      - "{{ web_group }}"
    keys:
      - "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
      - "{{ lookup('file', '~/.ssh/gitlab.pub') }}"

  - name: "{{ admin_user }}"
    groups:
      - sudo
    keys:
      - "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
      - "{{ lookup('file', '~/.ssh/gitlab.pub') }}"
```
3. Add these variables to Gitlab CI :
- ANSIBLE_SSHKEY : the key you generated for gitlab ci
- VAULT_PASS : your vault pass
- PRODUCTION_DOMAIN : domain.fr
- STAGING_DOMAIN : staging.domain.fr

4. Configure whitesource renovate for dependency updates, auto-merge enabled (Dependabot method in dependabot branch : No auto-merge)
If you don't want to manage a renovate-runner instance, you can leverage Gitlab CI schedules to do so by creating a new repo for your renovate runner and add a .gitlab-ci.yml :
```
include:
    - project: 'renovate-bot/renovate-runner'
      file: '/templates/renovate-dind.gitlab-ci.yml'
```
To trigger it, you create a cron (Gitlab CI/CD > Schedules) in this repo to run regularly
It will then create a configuration issue or MR in your repositories
Follow instructions here : https://gitlab.com/renovate-bot/renovate-runner/

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Open an issue if you face a bug or you have a feature ideas

## Contributing
You are free to contribute to this project, don't hesitate to fork it and provide enhancements

## Acknowledgment
Not affiliated with Roots project (but very grateful for their work !)

## License
MIT license
